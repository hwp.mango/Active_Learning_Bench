# Active Learning Benchmark
The basic premise of active learning is that large amounts of unlabelled data is often easy to come by, while labelling is a tedious and costly effort. 
Active learning aims to optimize network performance using a limited dataset by carefully selecting which data samples should be labelled for training.
This repository provides a tool to test various (geometric) active learning approaches. Tests are conducted on the CIFAR-10 dataset.
In each iteration a model is trained from scratch for 200 epochs on all training samples that have been labelled up to this point.
After each epoch network performance is estimated by testing the model on a validation set.
After training is completed the model is reverted to the checkpoint, where validation loss was minimal.
Then the model is used on a test set and test accuracy, confusion matrix and other parameters are logged.
To select new samples for labelling all training samples are projected into a vector space using their network activations at a selected layer. 
Then one of the available sampling strategies is used to add a batch of newly labelled samples to the training process.
For more information on the available labelling strategies and how to add costum labelling strategies refer to the [labeling strategy section](Documentation/Strategies.md).

For information on the available model architectures and how to add new architectures refer to the [model section](/Documentation/Models.md).
Finally there is an [evaluation section](/Documentation/Evaluation.md) presenting some of the results gathered during the creation of this repository.

# Table of Contents
1. [Installation](README.md#installation)
2. [Usage/Parameters](README.md#usageparameters)
   1.  [Experiments](README.md#experiments)
   2.  [Create Job Queues](README.md#create-job-queues)
   3.  [Plot Creation](README.md#plot-creation)
3. [Sampling Strategies](Documentation/Strategies.md#sampling-strategies)
   1.  [Random Sampling](Documentation/Strategies.md#random-sampling)
   2.  [Low Confidence Sampling](Documentation/Strategies.md#low-confidence-sampling)
   3.  [Greedy K Center](Documentation/Strategies.md#greedy-k-center)
   4.  [Spatial Loss Sampling](Documentation/Strategies.md#spatial-loss-sampling)
   5.  [Implementing New Strategies](Documentation/Strategies.md#implementing-new-strategies)
4. [Models](Documentation/Models.md#models)
   1.  [Selecting Target Layer](Documentation/Models.md#selecting-target-layer)
       1.  [Simple CNN](Documentation/Models.md#simple-cnn)
       2.  [ResNet18](Documentation/Models.md#resnet18)
   2.  [Adding Your Own Models](Documentation/Models.md#adding-your-own-models)
5. [Evaluation](Documentation/Evaluation.md#evaluation)
   1.  [Pitfalls](Documentation/Evaluation.md#pitfalls)
       1.  [Iterative Training](Documentation/Evaluation.md#iterative-training)
       2.  [Early Stopping](Documentation/Evaluation.md#early-stopping)
   2.  [Lessons Learned](Documentation/Evaluation.md#lessons-learned)
       1.  [Data Augmentation](Documentation/Evaluation.md#data-augmentation)
       2.  [Model Architecture](Documentation/Evaluation.md#model-architecture)
       3.  [Budget](Documentation/Evaluation.md#budget)
       4.  [Target Layer](Documentation/Evaluation.md#target-layer)
       4.  [Dataset](Documentation/Evaluation.md#dataset)
   3.  [Reproducibility](Documentation/Evaluation.md#reproducibility)


## Installation
```
pip install -r requirements.txt
```
## Usage/Parameters
### Experiments
Start an experiment with:
```
python active_learning_benchmark.py run --[Parameter] [value]
```
Current parameters are:
```
    --labeling_strategy $LABELING_STRATEGY
        Choose a strategy to select new samples. 
        Options: random_sampling, greedy_k_center, spatial_loss_sampling,
                 low_confidence_sampling
    --logfile $LOGFILE
        Filename for the json log created for this run
    --initial_training_size $INITIAL_TRAINING_SIZE
        Number of initially labelled samples
    --batch_size $BATCH_SIZE
        Number of samples per gradient update
    --budget $BUDGET
        Number of samples added in each iteration
    --iterations $ITERATIONS
        Maximum number of labeling iterations
    --target_layer $TARGET_LAYER
        Layer at which activations are extracted
    --model $MODEL_ARCHITECTURE
        Model architecture used for training.
        "simpleCNN" for a small convolutional network or "resnet18" for ResNet18 
    --data_augmentation
        Activate data augmentation during training
    --vis
        Activate visualization for network activations in 2-D space using PCA
```
Information of available target layers for each model architecture is provided in the [model section](/Documentation/Models.md#selecting-target-layer).
### Create Job Queues
While testing it can be helpful to queue up multiple test runs at once and then just letting them run on their own.
You can use the job_scheduler.py script to create and run such job queues.

To create or extend a job queue run: 
```
python job_queue.py add_new_jobs --[Parameter] [value]
```
Parameters for this function work mostly like the parameters for a single experiment run, but expect lists of setting 
values instead of single values. The resulting queue will contain jobs for all possible combinations of the provided settings.

```
--json_queue 
Expects a path to an existing or desired queue file.
If an existing queue file is provided new jobs will be appended to it.
--strategies "[$sampling_strategies]"
Default: "['random_sampling']"
--initial_training_splits "[$initial_split_sizes]"
Default: "[5000]"
--batch_sizes "[$batch_sizes]"
Default: "[128]"
--target_layers "[$target_layers]"
Default: "['fc3']"
--budgets "[$budgets]"
Default: "[5000]"
--data_augmentations
Default: "[True]"
--model: $model_architecture
This parameter only takes a single model type at once.
This is to avoid clashes with the target_layers parameter, 
as different models do not share the same layers.
Default: "simplecnn"
--runs $number_of_runs_per_setting_combination
Number of runs that will be added for each unique setting combination.
Default: 5

Note: The number of iterations is automatically inferred from budgets and 
inital_training_splits, such that the total number of labelled samples by the 
end of a run does not exceed the TOTAL_BUDGET, set as 40000 samples by default.
```

To start working on a job queue call:
```
python job_queue.py run_queue $json_queue_filepath
```
Experiments will be conducted in sequential order. Once a job is completed the queue file is updated allowing you to 
stop / resume the queue, while keeping track of progress.
You can split a queue file into multiple parts using:
```
python job_queue.py split_workload --json_queue $json_queue_filepath 
                                   Filename of the targeted queue
                                   --num_splits $number_of_desired_splits 
                                   Number of created queue splits
                                   --keep_original_file
                                   Flag to keep the original queue file
```
The created files are named after the original, having \_part_$part_index appended to their end.


### Plot Creation

Start by merging log files of runs with identical settings:
```
python evaluation.py merge
```
You can automatically create a variety of plots for your merged log files.
Filenames and directories will automatically be selected, based on the given data:
```
python evaluation.py single_plot --merged_logfile $merged_log_filename

Optional flags:

--plot_individal_runs
Creates additional plots for each run in your logfile.

--exclude_plot_types "[$unwanted_plot_types]"
Plot types are: 
['accuracy', 'class distribution', 'class distribution information gain',
 'confusion matrix', 'distribution recall correlation',
 'distribution precision correlation', 'distribution accuracy correlation']
```
To compare the results for multiple values of a given setting, use:
```
python evaluation.py multi_plot --examined_setting $setting_in_which_runs_differ 
                                --base_settings $dict_of_remaining_settings or
                                  $log_filename_with_desired_settings

Optional flags:

--ignored_settings "[$settings_that_may_differ]"
By default this routine will only use log files that differ exclusively in
the examined_setting. Log files that additionally differ in any of the ignored
settings will still be considered.

--exclude_plot_types "[$unwanted_plot_types]"
Plot types are ['accuracy','class distribution information gain']
```
To quickly get all single setting plots for your runs, use:
```
python evaluation.py single_plot_all 

Optional flags function identically to the single_plot flags.

--plot_individal_runs
Creates additional plots for each run in your logfile.

--exclude_plot_types "[$unwanted_plot_types]"
Plot types are:
['accuracy', 'class distribution', 'class distribution information gain',
'confusion matrix', 'distribution recall correlation',
'distribution precision correlation', 'distribution accuracy correlation']
```