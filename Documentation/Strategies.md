# Sampling Strategies

There are currently four different sampling strategies implemented in this repository.
These strategies are used to mark a number of new samples for labeling after each completed training iteration.

## Random Sampling 

Random sampling is the baseline strategy we compare against in our experiments. Here new samples for labelling are chosen at random.

## Low Confidence Sampling

Low confidence sampling is a basic sampling strategy based on network confidence when classifying unlabelled samples.
This strategy selects the samples the network is most uncertain about. Uncertainty of a sample is inferred from the softmax activations of the
last network layer as `1 - max(softmax_activations)` - Or the lower the confidence in the class the sample has been classified as, the higher the uncertainty.

An alternative way to define uncertainty is as the entropy of the softmax activations.

## Greedy K Center

Greedy k center is the greedy implementation of the strategy presented in this [paper](https://arxiv.org/pdf/1708.00489.pdf). This is the first approach using the created vector representation. 
The basic idea is to find a subset of the dataset such that all data points are closely enveloped by a labelled sample, thereby finding a set that generalizes the entire dataset.
Finding an optimal selection is NP-hard, however the paper proposes additional optimizations that provide results closer to the optimal solution and have a strategy to ignore outliers.
More information is provided at the [repository of the authors](https://github.com/ozansener/active_learning_coreset).  

## Spatial Loss Sampling 

Spatial loss sampling aims to map network performance to the spatial domain to select samples in regions where network performance is poor.
A heuristic for network performance issues at a given point $`p`$ is calculated as the cross entropy loss of all labelled samples $`L`$ weighted with their inverted squared distance to said point. 
These weighting factors $`w`$ are also normalized to ensure points with many close neighbours do not get favored over points in less dense areas.

$`
w_i = \frac {1}{\|L_i - p\|_2^2 * \sum_j^{|L|} \frac{1}{\|L_j -p\|_2^2}}
`$

Therefore network performance issues at $`p`$ are estimated as:

$`NPI(p) = \sum_i^{|L|} w_i * CrossEntropyLoss(L_i)`$

To select new samples all unlabelled samples are ranked by their $`NPI`$. Then new samples are selected randomly using the 
$`NPI`$ ranking as a probability distribution.

## Implementing New Strategies

You can add your own custom sampling strategy by adding a sampling method to the ActiveLearningBench class in
[active_learning_benchmark.py](/active_learning_benchmark.py). The function signature for all sampling strategies looks like this:

```python
def custom_sampling_strategy(self, activations, losses, confidences):
```

The sampling strategy will be called after each completed full training iteration - refer to the run function.

```python
self.__getattribute__(self.labeling_strategy)(activations, losses, confidences)
```

Each of these inputs is a torch tensor owned by cuda containing respective data on all training samples (labelled, unlabelled and validation set).

- activations: contains the network activations extracted at the target layer
- losses: contains the cross entropy losses 
- confidences: contains the softmax activations extracted at the end of the network


```python
self.unlabelled_idx
```
and 
```python
self.labelled_idx
```
contain the index lists for unlabelled and labelled samples.
You can use these lists to get data of the unlabelled/labelled samples. E.g.:
```python
unlabelled_activations_data = activations[self.unlabelled_idx]
```
You can get the sampling budget with 
```python
self.budget
```
. By the end of your function you need to update the index lists and
the 
```python
self.train_loader
```
with the indices of the newly selected samples. You can use the 
```python
def update_train_loader(self, added_samples):
```
function to do this, which takes a list of indices you want to add
to the training process. Alternatively take a look at the greedy_k_center implementation for a manual way.
To use your new strategy simple provide the strategy flag with your custom function name.
