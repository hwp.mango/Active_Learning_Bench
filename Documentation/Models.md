# Models

Currently there are two model architectures available for experiments: A very basic CNN and a version of ResNet18 modified for 
the CIFAR-10 inputs. For either model each layer can be
chosen as a target layer. Activations are extracted using a pytorch forward hook. Currently the inputs to the 
target layer are tracked, however with a few tweaks you can also track layer outputs.

## Selecting Target Layer 

The desired target layer is set by the target_layer flag and is referenced by name. 

### Simple CNN

Possible target layers and dimensionality of layer input.

| Layer Name    | Number Of Features |
| :-----------: |:------------------:|
| conv1         | 3072               |  
| pool          | 1600               |  
| conv2         | 1176               |  
| fc1           | 400                |  
| fc2           | 120                |  
| fc3           | 84                 |

### ResNet18

Possible target layers and dimensionality of layer input.

| Layer Name         | Number Of Features |
|:------------------:|:------------------:|
| conv1              | 3072               |
| bn1                | 65536              |
|layer1_0_conv1      | 65536              |
|layer1_0_bn1        | 65536              |
|layer1_0_conv2      | 65536              |
|layer1_0_bn2        | 65536              |
|layer1_1_conv1      | 65536              |
|layer1_1_bn1        | 65536              |
|layer1_1_conv2      | 65536              |
|layer1_1_bn2        | 65536              |
|layer2_0_conv1      | 65536              |
|layer2_0_bn1        | 32768              |
|layer2_0_conv2      | 32768              |
|layer2_0_bn2        | 32768              |
|layer2_0_shortcut_0 | 65536              |
|layer2_0_shortcut_1 | 32768              |
|layer2_1_conv1      | 32768              |
|layer2_1_bn1        | 32768              |
|layer2_1_conv2      | 32768              |
|layer2_1_bn2        | 32768              |
|layer3_0_conv1      | 32768              |
|layer3_0_bn1        | 16384              |
|layer3_0_conv2      | 16384              |
|layer3_0_bn2        | 16384              |
|layer3_0_shortcut_0 | 32768              |
|layer3_0_shortcut_1 | 16384              |
|layer3_1_conv1      | 16384              |
|layer3_1_bn1        | 16384              |
|layer3_1_conv2      | 16384              |
|layer3_1_bn2        | 16384              |
|layer4_0_conv1      | 16384              |
|layer4_0_bn1        | 8192               |
|layer4_0_conv2      | 8192               |
|layer4_0_bn2        | 8192               |
|layer4_0_shortcut_0 | 16384              |
|layer4_0_shortcut_1 | 8192               |
|layer4_1_conv1      | 8192               |
|layer4_1_bn1        | 8192               |
|layer4_1_conv2      | 8192               |
|layer4_1_bn2        | 8192               |
|linear              | 512                |

## Adding Your Own Models

Adding different network architectures is straight forward. Make sure to use models that are built for the CIFAR inputs.
The ResNet architecture used in this repository is taken from [kuanglius pytorch-cifar repository](https://github.com/kuangliu/pytorch-cifar).
You can find a bunch of different architectures modified for the CIFAR-10 dataset there. 
                                          
1.  Construct your model at the beginning of the get_trained_model function in [active_learning_benchmark.py](/active_learning_benchmark.py)
    ```python
    def get_trained_model(self):
        # Initialize model and optimization tools.
        if self.model_type == "simplecnn":
            model = SimpleCNN()
        elif self.model_type == "my_custom_model_identifier":
            model = MyModel()
    ```

2.  Hook a layer of your network and calculate the number of expected input features to this layer at the beginning of the 
    create_vector_rep function by writing a custom function for your architecture.    
    ```python
    def create_vector_rep(self, model, criterion):
        model.eval()
        if type(model) == SimpleCNN:
            hook, num_features = self.hook_simple_cnn(model)
        elif type(model) == MyModel:
            hook, num_features = self.hook_my_model(model)
    ```
    Your custom function needs to get access to a layer based on the string passed to the target_layer flag.
    To get an idea how to do this for larger models, look at the hook_resnet function. Once you found your target layer
    you can use:
    
    ```python
    def hook_and_get_num_features(self, model, layer):
    ```    
    To get a hook and the corresponding number of input features, by passing your model and the target layer.

To use your model in experiments set the model flag to your model identifier.