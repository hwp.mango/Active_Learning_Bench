# Evaluation

This repository was created as a student project at TU Dresden in
cooperation with AI4BD to answer questions regarding the viability of
different active learning schemes for classification tasks. In most
active learning papers new strategies are tested and compared in a fixed
setting with optimized hyperparameters, which makes it difficult to
determine whether a strategy generally boosts performance or is only
viable under certain conditions. Therefore an important part of testing
was to analyse various training parameters to determine whether they
would influence the success of the tested strategies. The following
section will present some of the lessons learned during the creation of
this project.

## Pitfalls

This repository has undergone multiple iterations, therefore there are
testing results that are not reproducible with the current state of the
repository. Most of these changes seem quite intuitive in retrospect
however, I want to briefly discuss the 'pitfalls' I ran into and why I
think they were flawed attempts.

### Iterative Training

In the earliest iteration of this repository sampling was done
iteratively during the training process without retraining. Training
started with an initial training set for a fixed number of epochs, then
new samples were added by the different labeling strategies and training
continued for the fixed number of epochs again. This was repeated until
all samples were added to the training set. I started out like this
because the total training time when continuing training is cut down
significantly. However this approach comes with a lot of disadvantages.
First of deciding on the number of epochs between sampling phases non
trivial. Training too long on very few samples can send the network into
a bad performing direction where it does not recover from, whereas
training for to few epochs means that sampling decisions are made using
an insufficiently trained network which might lead to suboptimal
sampling decision. Secondly when training iteratively we do not get
information about the true potential of the currently sampled training
set. Lastly this is also not a realistic scenario. The baseline
assumption during this project is that training is considered cheap
whereas labelling is expensive, therefore cutting training time for
performance does not make sense. Running experiments like this led to
very poor performance of the
[greedy-k-center](Strategies.md#greedy-k-center) sampling strategy I was
testing back then in comparision to
[random sampling](Strategies.md#random-sampling). <br><img
src="Iterative%20Training/setting_evaluation_plots/Eval:Strategy_with:1000_1000_2_32_40_0.001_4/Accuracy%20Plot.png"
alt="Iterative Training Performance" width="40%"> <br> A likely reason
that greedy-k-center sampling was outperformed by random sampling is,
that it was creating an imbalanced class distribution. <br> <img
src="Iterative%20Training/setting_evaluation_plots/k-center-greedy-class-distribution.gif"
alt="Greedy-K-Center class distribution over iterations" width="40%">
<img
src="Iterative%20Training/setting_evaluation_plots/random-sampling-class-distribution.gif"
alt="Greedy-K-Center class distribution over iterations" width="40%">
<br> Left: Class distribution for greedy-k-center <br> Right: Class
distribution for random sampling

In further I will use information gain of the class distribution over an
even distribution to illustrate class imbalance: <br> <img
src="Iterative%20Training/setting_evaluation_plots/Eval:Strategy_with:1000_1000_2_32_40_0.001_4/Information%20Gain.png"
alt="Class Imbalance Illustration" width="40%"> <figcaption>Class
distribution as information gain - equivalent to the animations above.
</figcaption>

### Early Stopping

In the next project iteration I began retraining models from scratch
after each labelling phase. However to reduce training time I adopted
early stopping. While I believe that this can work out fine if you put
enough time and thought into it I ended up dropping it because I was
unable to find a reliable criterion to determine when the model reached
its full potential. I calculated a validation loss after each epoch - if
the validation loss had not improved for a set number of epochs I
stopped training and reverted the model to the state with minimal
validation loss. Finding a good threshold for the allowed number of
epochs without improvement was difficult, as models sometimes started
improving after long periods without progress. Setting the threshold too
high would compromise the potential time savings so in the current
iteration training is done for a fixed amount of epochs. After training
is complete the model is still reverted to the state where validation
loss was minimal.

## Lessons Learned

As a general lesson from this project one can conclude that choosing
which data points to label when labelling is considered a limited
resource can have a significant positive impact on network performance.
However the strategies tested here do not work unconditionally and
should be used with care. Employing these strategies in unfit conditions
might nullify their benefits completely or even cause adverse effects.
<br> Looking at the different strategies, the [confidence based
approach](Strategies.md#low-confidence-sampling) showed the most
promising results. There are also a lot of other approaches that build
on this concept (e.g. [CEAL](https://arxiv.org/pdf/1701.03551.pdf)) that
might be worth looking into.
[Greedy-k-center](Strategies.md#greedy-k-center) also showed good
results in many experiments, however it seems to be highly dependent on
having a well performing network to make good sampling decisions. In the
[related paper](https://arxiv.org/pdf/1708.00489.pdf) the authors
describe an optimization of the greedy approach which would be
interesting to test when looking for a good geometric approach. The
[spatial loss sampling](Strategies.md#spatial-loss-sampling) strategy
proposed in this repository could not consistently outperform random
selection therefore it is not recommended for usage in its current form.

### Data Augmentation

Data augmentation turns out to have a large impact on the active
learning strategies tested. The data augmentation used consists of a
random crop with zero-padding of size four, as well as random horizontal
flips. Not only does it generally improve network performance, it also
is necessary for the greedy-k-center and low confidence approaches to
reach more of their potential and allows them to outperform random
sampling at an earlier stage. <br> <img
src="Lessons%20Learned/Eval:Strategy_with:5000_5000_128_7_linear_resnet18/Accuracy%20Plot.png"
alt="Accuracy Plot No Data Augmentation" width="40%"> <img
src="Lessons%20Learned/Eval:Strategy_with:5000_5000_128_7_linear_resnet18_data_augmentation/Accuracy%20Plot.png"
alt="Accuracy Plot Data Augmentation" width="40%"> <br> Runs on
ResNet18, using "linear" as [target layer](Models.md#resnet18).
Initially 5000 samples labelled with a budget of 5000 samples per
iteration. <br> Left: No data augmentation. <br> Right: Data
augmentation.

As a side effect the sampled training sets also have a much more
balanced class distribution when using data augmentation. <br> <img
src="Lessons%20Learned/Eval:Strategy_with:5000_5000_128_7_linear_resnet18/Information%20Gain.png"
alt="Accuracy Plot No Data Augmentation" width="40%"> <img
src="Lessons%20Learned/Eval:Strategy_with:5000_5000_128_7_linear_resnet18_data_augmentation/Information%20Gain.png"
alt="Accuracy Plot Data Augmentation" width="40%"> <br>
Left: No data augmentation. <br> Right: Data augmentation.

### Model Architecture

The two model architecture presented here vary quite drastically.
ResNet18 is a complex and very capable model for the CIFAR10 task
whereas the simpleCNN used only consists of a few layers, so it can
hardly compete with ResNet. While this is unsurprising the interesting
point is that the given active learning strategies do not seem to work
at all when applied to the small architecture, being outperformed by
random sampling. <br> <img
src="Lessons%20Learned/Eval:Strategy_with:5000_5000_128_7_4_simplecnn_data_augmentation/Accuracy%20Plot.png"
alt="Accuracy Plot No Data Augmentation" width="40%"> <img
src="Lessons%20Learned/Eval:Strategy_with:5000_5000_128_7_linear_resnet18_data_augmentation/Accuracy%20Plot.png"
alt="Accuracy Plot Data Augmentation" width="40%"> <br> Runs using data
augmentation with "fc3" and "linear" as
[target layers](Models.md#selecting-target-layer). Initially 5000
samples labelled with a budget of 5000 samples per iteration. <br> Left:
SimpleCNN. <br> Right: ResNet18. <br> Once again poor strategy
performance seems to be correlated with high class distribution
imbalance: <br> <img
src="Lessons%20Learned/Eval:Strategy_with:5000_5000_128_7_4_simplecnn_data_augmentation/Information%20Gain.png"
alt="Accuracy Plot No Data Augmentation" width="40%"> <img
src="Lessons%20Learned/Eval:Strategy_with:5000_5000_128_7_linear_resnet18_data_augmentation/Information%20Gain.png"
alt="Accuracy Plot Data Augmentation" width="40%"> <br> Left: SimpleCNN.
<br> Right: ResNet18. <br> I also tested VGG16 as a more competitive
architecture to ResNet18 which showed very similar results to ResNet18.
Therefore these strategies do not seem to generally behave differently
based on architecture but more on overall model capability.

### Budget

The number of newly labelled samples does not seem to significantly
impact any of the active learning strategies.

<br> <img
src="Lessons%20Learned/Eval:Budget_with:greedy_k_center_5000_128_linear_resnet18_data_augmentation/Accuracy%20Plot.png"
alt="Budget Test Greedy-K-Center" width="40%"> <img
src="Lessons%20Learned/Eval:Budget_with:low_confidence_sampling_5000_128_linear_resnet18_data_augmentation/Accuracy%20Plot.png"
alt="Budget Test Greedy-K-Center" width="40%"> <br> <img
src="Lessons%20Learned/Eval:Budget_with:spatial_loss_sampling_5000_128_linear_resnet18_data_augmentation/Accuracy%20Plot.png"
alt="Budget Test Greedy-K-Center" width="40%"> <img
src="Lessons%20Learned/Eval:Budget_with:random_sampling_5000_128_linear_resnet18_data_augmentation/Accuracy%20Plot.png"
alt="Budget Test Greedy-K-Center" width="40%"> <br> Runs using data
augmentation with "linear"
[target layers](Models.md#selecting-target-layer) on ResNet18 and 5000
samples labelled initially. <br> Top left: Greedy-k-center <br> Top
right: Low confidence sampling <br> Bottom left: Spatial loss sampling
<br> Bottom right: Random sampling <br>

### Target Layer

The chosen target layer only affects strategies that make use of the
vector representation. For spacial loss sampling it does not seem to
have a significant impact. However for the greedy-k-center method it
seems to work better when using the last fully connected layer of the
network. Choosing this layer was also recommended in the [original
paper](https://arxiv.org/pdf/1708.00489.pdf). In my experiments I only
tested two different layers so it could be interesting to test more
layers for the greedy-k-center approach. <br> <img
src="Lessons%20Learned/Eval:Target%20Layer_with:spatial_loss_sampling_5000_5000_128_7_resnet18_data_augmentation/Accuracy%20Plot.png"
alt="Budget Test Greedy-K-Center" width="40%"> <img
src="Lessons%20Learned/Eval:Target%20Layer_with:greedy_k_center_5000_5000_128_7_resnet18_data_augmentation/Accuracy%20Plot.png"
alt="Budget Test Greedy-K-Center" width="40%"> <br> Runs on ResNet18
with data augmentation. Initially 5000 samples labelled with a budget of
5000 samples per iteration. <br> Left: Spatial loss sampling <br> Right:
Greedy-k-center

### Dataset

To this point all experiments have been conducted on the CIFAR10
dataset. To see if the observed results can be repeated on other datasets
I also ran an experiment on MNIST. The results mostly carry over, 
however on MNIST spatial loss sampling seems
to be a significant improvement over random sampling. <br> <img
src="Lessons%20Learned/Eval:Strategy_with:2000_1000_128_19_linear_resnet18_MNIST_data_augmentation/Accuracy%20Plot.png"
alt="MNIST test" width="40%"> <img
src="Lessons%20Learned/Eval:Strategy_with:2000_1000_128_19_linear_resnet18_MNIST_data_augmentation/Information%20Gain.png"
alt="MNIST test" width="40%"> <br> Runs on ResNet18 with data augmentation
testing the MNIST dataset. Initially 1000 samples labelled with a budget
of 2000 samples per iteration.

## Reproducibility

To make reproduction of these results easier I am listing the parameters
that were used across all experiments. If anything remains unclear take
a look at [active_learning_benchmark.py](/active_learning_benchmark.py)
for the original implementation. All experiments were conducted on the
CIFAR10 dataset taken of torchvision, which was normalized using the mean
and standard deviation of the trainings set. 5000 samples of the
trainings set were reserved for the validation set. Training started out
on 5000 initially labelled samples. Between the addition of new samples
a new model was trained for 200 epochs using a batch size of 128. After
training the model state was reverted to the point were its validation
loss was minimal. Optimization was done using the rmsprop optimizer with
learning rate 1e-4 momentum of 0.9 and weight decay of 5e-4. All
experimental results presented have been gathered and averaged over at
least 3 runs - error bars are standard deviation.